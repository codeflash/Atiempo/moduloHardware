-- MySQL Script generated by MySQL Workbench
-- 09/20/16 11:35:31
-- Model: New Model    Version: 1.0
-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema atiempo
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema atiempo
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `atiempo` DEFAULT CHARACTER SET utf8 ;
USE `atiempo` ;

-- -----------------------------------------------------
-- Table `atiempo`.`ciudades`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `atiempo`.`ciudades` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(200) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `atiempo`.`paises`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `atiempo`.`paises` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(200) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `atiempo`.`estados`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `atiempo`.`estados` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(200) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `atiempo`.`empresas`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `atiempo`.`empresas` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `correo` VARCHAR(200) NULL,
  `contrasenia` TEXT NULL,
  `descripcion` VARCHAR(200) NULL,
  `nombre` VARCHAR(200) NULL,
  `telefono` VARCHAR(45) NULL,
  `direccion` VARCHAR(200) NULL,
  `RUC` VARCHAR(100) NULL DEFAULT NULL,
  `idCiudad` INT NOT NULL,
  `idPais` INT NOT NULL,
  `idEstado` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_empresas_ciudad1_idx` (`idCiudad` ASC),
  INDEX `fk_empresas_pais1_idx` (`idPais` ASC),
  INDEX `fk_empresas_estado1_idx` (`idEstado` ASC),
  UNIQUE INDEX `correo_UNIQUE` (`correo` ASC),
  CONSTRAINT `fk_empresas_ciudad1`
    FOREIGN KEY (`idCiudad`)
    REFERENCES `atiempo`.`ciudades` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_empresas_pais1`
    FOREIGN KEY (`idPais`)
    REFERENCES `atiempo`.`paises` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_empresas_estado1`
    FOREIGN KEY (`idEstado`)
    REFERENCES `atiempo`.`estados` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `atiempo`.`vehiculos`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `atiempo`.`vehiculos` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `descripcion` VARCHAR(200) NULL,
  `placa` VARCHAR(45) NOT NULL,
  `idEmpresa` INT NOT NULL,
  `longitud` FLOAT NULL,
  `latitud` FLOAT NULL,
  `velocidad` FLOAT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `placa_UNIQUE` (`placa` ASC),
  INDEX `fk_vehiculos_empresas1_idx` (`idEmpresa` ASC),
  CONSTRAINT `fk_vehiculos_empresas1`
    FOREIGN KEY (`idEmpresa`)
    REFERENCES `atiempo`.`empresas` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `atiempo`.`rutas`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `atiempo`.`rutas` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `descripcion` VARCHAR(45) NULL,
  `ruta` BLOB NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `atiempo`.`detalleRuta`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `atiempo`.`detalleRuta` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `idEmpresa` INT NOT NULL,
  `idRuta` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_Empresas_has_Rutas_Rutas1_idx` (`idRuta` ASC),
  INDEX `fk_Empresas_has_Rutas_Empresas_idx` (`idEmpresa` ASC),
  CONSTRAINT `fk_Empresas_has_Rutas_Empresas`
    FOREIGN KEY (`idEmpresa`)
    REFERENCES `atiempo`.`empresas` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Empresas_has_Rutas_Rutas1`
    FOREIGN KEY (`idRuta`)
    REFERENCES `atiempo`.`rutas` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `atiempo`.`usuarios`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `atiempo`.`usuarios` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(100) NULL,
  `apellidos` VARCHAR(100) NULL,
  `alias` VARCHAR(100) NULL,
  `contrasenia` TEXT NULL,
  `correo` VARCHAR(200) NULL,
  `tipo` ENUM('E', 'R') NULL DEFAULT 'R',
  `foto` TEXT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `correo_UNIQUE` (`correo` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `atiempo`.`valoraciones`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `atiempo`.`valoraciones` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `puntuacion` INT(5) NULL,
  `descripcion` VARCHAR(200) NULL,
  `fecha` DATETIME NULL,
  `idUsuario` INT NOT NULL,
  `empresas_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_valoraciones_usuarios1_idx` (`idUsuario` ASC),
  INDEX `fk_valoraciones_empresas1_idx` (`empresas_id` ASC),
  CONSTRAINT `fk_valoraciones_usuarios1`
    FOREIGN KEY (`idUsuario`)
    REFERENCES `atiempo`.`usuarios` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_valoraciones_empresas1`
    FOREIGN KEY (`empresas_id`)
    REFERENCES `atiempo`.`empresas` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `atiempo`.`fotosEmpresa`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `atiempo`.`fotosEmpresa` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `fotos` TEXT NULL,
  `idEmpresas` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_fotosEmpresa_empresas1_idx` (`idEmpresas` ASC),
  CONSTRAINT `fk_fotosEmpresa_empresas1`
    FOREIGN KEY (`idEmpresas`)
    REFERENCES `atiempo`.`empresas` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
